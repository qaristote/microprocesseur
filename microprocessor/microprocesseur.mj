(* --------------------------- VARIABLES GLOBALES --------------------------- *)

const ops_nbr_length = 7 (* devrait suffire jusqu'à 64 opérations par seconde *)
      		       

(* ---------------------------------- UAL ---------------------------------- *)

zeros<n>() = o:[n] where
    if n = 0 then
        o = []
    else
	o = 0.zeros<n-1>()
    end if
end where

ones<n>() = o:[n] where
    if n = 0 then
        o = []
    else
	o = 1.ones<n-1>()
    end if
end where

multi_and<n>(a:[n], b:[n]) = o:[n] where
   if n = 0 then
       o = []
   else 
       o = (a[0] & b[0]).multi_and<n-1>(a[1..], b[1..])
   end if
end where

multi_or<n>(a:[n], b:[n]) = o:[n] where
    if n = 0 then
        o = [];
    else 
        o = (a[0] + b[0]).multi_or<n-1>(a[1..], b[1..])
    end if
end where


multi_xor<n>(a:[n], b:[n]) = o:[n] where
    if n = 0 then
        o = [];
    else
	o = (a[0] ^ b[0]).multi_xor<n-1>(a[1..],b[1..])
    end if
end where

(* bizzarement le cas n = 0 fait tout bugger *)
multi_not<n>(a:[n]) = o:[n] where
    if n = 1 then
        o = not(a)
    else
	o = not(a[0]).multi_not<n-1>(a[1..])
    end if
end where

multi_nor<n>(a:[n], b:[n]) = o:[n] where
    o = multi_not<n>(multi_or<n>(a, b))
end where

multi_mux<n>(c, a:[n], b:[n]) = o:[n] where
    if n = 0 then
        o = []
    else
	o = mux(c, a[0], b[0]).multi_mux<n-1>(c, a[1..n-1], b[1..n-1])
    end if
end where

add<n>(a:[n], b:[n], c_in) = (o:[n], c_out) where
   if n = 0 then
       o = [] ;
       c_out = c_in
   else
       (tail, c) = add<n-1>(a[1..], b[1..], c_in) ; 
       tmp = a[0] ^ b[0] ;
       o = (tmp ^ c).tail ;
       c_out = (tmp & c) + (a[0] & b[0])
   end if
end where

(* a - b *)
sub<n>(a:[n], b:[n]) = o:[n] where
    (o, c) = add<n>(a, multi_not<n>(b), 1)
end where

(* a >= 0 *)
gez<n>(a:[n]) = o where
    (* a >= 0 ssi son bit de signe est nul *)
    o = not(a[0])
end where

(* a = 0 *)
ez<n>(a:[n]) = o where
    if n = 1 then
        o = not(a[0])
    else
        o = not(a[0]) & ez<n-1>(a[1..])
    end if
end where

(* a > O *)
gz<n>(a:[n]) = o where
    o = not(a[0] + ez<n>(a))
end where

(* a < 0 *)
lz<n>(a:[n]) = o where
    o = a[0]
end where

(* a <= 0 *)
lez<n>(a:[n]) = o where
    o = a[0] + ez<n>(a)
end where


(* On fait le shift p fois *)
sll<n, m, p>(a:[n], shamt:[m]) = o:[n] where
    if m = 0 then
        o = a
    else
        o = sll<n, m-1, 2*p>(multi_mux<n>(shamt[m-1],
					  a[..n-(p+1)].zeros<p>(),
					  a),
			     shamt[..m-2])
    end if
end where

srl<n, m, p>(a:[n], shamt:[m]) = o:[n] where
    if m = 0 then
        o = a
    else
        o = srl<n, m-1, 2*p>(multi_mux<n>(shamt[m-1],
					  zeros<p>().a[p..],
					  a),
			     shamt[..m-2])
    end if
end where

sra<n, m, p>(a:[n], shamt:[m]) = o:[n] where
    if m = 0 then
        o = a
    else
        o = sra<n, m-1, 2*p>(multi_mux<n>(shamt[m-1],
					  ones<p>().a[p..],
					  a),
			     shamt[..m-2])
    end if
end where


(* ------------------------------ INSTRUCTIONS ------------------------------ *)

addi(s:[16], imm:[16]) = o:[16] where
    (o, c) = add<16>(s, imm, 0)
end where

bltz(s:[16], imm:[16], PC:[16]) = new_PC:[16] where
    (new_PC, c) = add<16>(PC,
			  multi_mux<16>(lz<16>(s), imm, 0b0000000000000001),
			  0)
end where

beq(s:[16], t:[16], imm:[16], PC:[16]) = new_PC:[16] where
    e = sub<16>(s, t) ;
    (new_PC, c) = add<16>(PC,
			  multi_mux<16>(ez<16>(e), imm, 0b0000000000000001),
			  0)
end where

bne(s:[16], t:[16], imm:[16], PC:[16]) = new_PC:[16] where
    e = sub<16>(s, t) ;
    (new_PC, c) = add<16>(PC,
			  multi_mux<16>(ez<16>(e), 0b0000000000000001, imm),
			  0)
end where

j(label:[26], PC:[32]) = new_PC:[32] where
    (* le compilateur assembleur rajoute 0x00400024 par défaut *)
    new_PC = sub<32>(PC[0..3].label.00, 0x00400024)
end where

type_i(opcode:[6],
       rs:[5],
       s:[16],
       rt:[5],
       t:[16],
       imm:[16],
       PC:[16]) = (write_enable,
		   write_value:[16],
		   new_PC:[16]) where

    o_addi = addi(s, imm) ;
    o_bltz = bltz(s, imm, PC) ;
    o_beq  = beq(s, t, imm, PC) ;
    o_bne  = bne(s, t, imm, PC) ;
    o_lui  = imm ;

    new_PC_v1 = multi_mux<16>(opcode[5], o_bne, o_beq) ;
    new_PC_v2 = multi_mux<16>(opcode[3], new_PC_v1, o_bltz) ;
    (inc_PC, c) = add<16>(PC, 0b0000000000000001, 0) ;

    write_enable  = opcode[2] ;
    write_value   = multi_mux<16>(opcode[5], o_lui, o_addi) ;
    new_PC        = multi_mux<16>(opcode[2], inc_PC, new_PC_v2)
end where

type_r(s:[16],
       t:[16],
       shamt:[5],
       funct:[6]) = write_value:[16] where

    o_add = addi(s, t) ;
    o_sll = sll<16, 5, 1>(t, shamt) ;
    o_srl = srl<16, 5, 1>(t, shamt) ;

    write_value_v1 = multi_mux<16>(funct[0], o_add, o_sll) ;
    write_value    = multi_mux<16>(funct[4], o_srl, write_value_v1)
end where

(* -------------------------------- MÉMOIRE -------------------------------- *)

register_access(rs:[5],
		rt:[5],
		write_enable,
		write_address:[5],
		write_word:[16]) = (t0:[16],
				    t1:[16],
				    t2:[16],
				    t3:[16],
				    t4:[16],
				    t5:[16],
				    t6:[16],
				    t7:[16],
				    rs_val:[16],
				    rt_val:[16]) where
    real_write_address = write_address[1..] ;	
    t0 = ram<4, 16>(0b1000, write_enable, real_write_address, write_word) ;
    t1 = ram<4, 16>(0b1001, write_enable, real_write_address, write_word) ;
    t2 = ram<4, 16>(0b1010, write_enable, real_write_address, write_word) ;
    t3 = ram<4, 16>(0b1011, write_enable, real_write_address, write_word) ;
    t4 = ram<4, 16>(0b1100, write_enable, real_write_address, write_word) ;
    t5 = ram<4, 16>(0b1101, write_enable, real_write_address, write_word) ;
    t6 = ram<4, 16>(0b1110, write_enable, real_write_address, write_word) ;
    t7 = ram<4, 16>(0b1111, write_enable, real_write_address, write_word) ;
    rs_val = multi_mux<16>(ez<5>(rs),
		           0b0000000000000000,
    	   	 	   ram<4, 16>(rs[1..],
			   	       write_enable,
				       real_write_address,
				       write_word)) ;
    rt_val = multi_mux<16>(ez<5>(rt),
		           0b0000000000000000,
    	   	       	   ram<4, 16>(rt[1..],
		 	              write_enable,
				      real_write_address,
				      write_word)) ;
end where

PC_access(new_PC:[32]) = old_PC:[32] where
    old_PC = ram<1, 32>(0, 1, 0, new_PC)
end where

ops_to_do() = o:[ops_nbr_length] where
    o = 0b0110001 (* 50 *)
end where

ops_left(reset) = ops:[ops_nbr_length] where
    (ops, c) = add<ops_nbr_length>(
		   ram<1, ops_nbr_length>(0,
					  1,
				          0,
				          multi_mux<ops_nbr_length>(reset,
								    ops_to_do(),
							    	    ops)),
      		    0b1111111,
		    0)
end where

ops_done() = ops:[ops_nbr_length] where
    (ops, c) = add<ops_nbr_length>(ram<1, ops_nbr_length>(0, 1, 0, ops),
				   0b0000000,
				   1)
end where

(* -------------------------- PROGRAMME PRINCIPAL -------------------------- *)

execute_command(command:[32], old_PC:[32]) = (t0:[16],
			      		      t1:[16],
			         	      t2:[16],
					      t3:[16],
					      t4:[16],
					      t5:[16],
					      t6:[16],
					      t7:[16],
					      new_PC:[32]) where
    opcode         = command[0..5]   ;
    rs 		   = command[6..10]  ;
    rt 		   = command[11..15] ;
    rd 		   = command[16..20] ;
    shamt 	   = command[21..25] ;
    funct  	   = command[26..31] ;
    imm            = command[16..31] ;
    pseudo_address = command[6..31]  ;
    
    (* les valeurs des registres renvoyées ne sont pas les résultats de la
       commande mais les valeurs utilisées pour calculer ces résultats ;
       on est donc décalé mais en pratique ce n'est pas un problème car
       l'éxecution d'une commande se fait très rapidement		   *)
    (t0,
     t1,
     t2,
     t3,
     t4,
     t5,
     t6,
     t7,
     rs_val,
     rt_val) = register_access(rs,
			       rt,
			       write_enable,
			       write_address,
			       write_value) ;

     (* En théorie on devrait faire ça mais en pratique PC est décalé de 1 octet
	(PC, c) = add<32>(old_PC, 0b00000000000000000000000000000100, 0) ; *)

     (* la valeur ajoutée à PC dans les branchements est divisée par 4 dans le
     	code assembleur compilé pour gagner 2 bits *)
     (write_enable_I,
      write_value_I,
      new_PC_I)        = type_i(opcode,
				rs, rs_val,
				rt, rt_val,
				imm,
				old_PC[14..29]) ;
     write_value_R     = type_r(rs_val, rt_val, shamt, funct) ;
     new_PC_J  	       = j(pseudo_address, old_PC) ;


     J = opcode[4] & not(opcode[5]) ;
     I = opcode[2] + opcode[3] + opcode[5] ;

     write_enable  = mux(J, 0, mux(I, write_enable_I, 1)) ;
     write_address = multi_mux<5>(I, rt, rd) ;
     write_value   = multi_mux<16>(I, write_value_I, write_value_R) ;
     (inc_PC, c)   = add<32>(old_PC, 0b00000000000000000000000000000100, 0) ;
     new_PC        = multi_mux<32>(J + I,
				   multi_mux<32>(J,
						 new_PC_J,
						 old_PC[..13].new_PC_I.00),
				   inc_PC)
end where

main() = (t0:[16],
          t1:[16],
	  t2:[16],
	  t3:[16],
	  t4:[16],
	  t5:[16],
	  t6:[16],
	  t7:[16]) where
    ops = ops_left(reset) ;
    reset = lz<ops_nbr_length>(ops) ;
    PC = PC_access(multi_mux<32>(reset,
				 0b00000000000000000000000000000000,
				 new_PC)) ;
    command = rom<30, 32>(PC[..29]) ;
    (t0, t1, t2, t3, t4, t5, t6, t7, new_PC) = execute_command(command, PC)
end where