	# 	 _____________________________________
	# 	|				      |
	# 	| Programme assembleur pour l'horloge |
	# 	|_____________________________________|
	#
	# On suppose que les registres suivants sont déjà initialisés comme
	# explicité :
	# 	$t0 : seconde (s) (0 -> 59)
	# 	$t1 : minute (min) (0 -> 59)
	# 	$t2 : heure (h) (0 -> 23)
	# 	$t3 : jour (j) (1 -> 30)
	# 	$t4 : mois (m) (1 -> 12)
	# 	$t5 : année (a) (0 -> 2^31-1)
	# 	$t6 : groupe de travailleurs (t) (0 -> 5)
	# 	$t7 : jour férié (f) (0 -> 6)
	# Le calendrier révolutionnaire soviétique fonctionne ainsi :
	# Chaque mois compte 30 jours, et les cinq ou six jours restants sont
	# ajoutés comme jours intermédiaires de congé (fériés), n'appartenant à
	# aucun des 12 mois et à aucune semaine. Ces jours sont :
	# 	le jour de Lénine, après le 30 janvier ($t7 = 1) ;
	# 	deux jours du travail, après le 30 avril ($t7 = 2, 3) ;
	# 	deux jours de l'industrie, après le 7 novembre $(t7 = 4, 5) ;
	# 	les années bissextiles, un jour supplémentaire après le 30
	#	février ($t7 = 6).
main :
	# s != 59 ?
	addi	$s0, $t0, -59
	# si oui, on incrémente seulement la seconde
	bltz	$s0, not_last_second		
	# sinon, on essaie d'incrémenter la minute
	lui 	$t0, 0		# s = 0
	
	# min != 59 ?
	addi 	$s0, $t1, -59
	# si oui, on incrémente seulement la minute
	bltz 	$s0, not_last_minute
	# sinon, on essaie d'incrémenter l'heure
	lui 	$t1, 0		# min = 0
	
	# h != 23 ?
	addi 	$s0, $t2, -23
	# si oui, on incrémente seulement l'heure
	bltz 	$s0, not_last_hour
	# si non, on essaie d'incrémenter le jour
	lui 	$t2, 0		# h = 0
	
	# j != 0 ?
	# si oui, le jour n'est pas férié
	bne	$t3, $zero, not_rest_day

	# sinon, le jour est férié et on lui accorde
	# un traitement particuluier
	
	# f = 2 (1er jour des travailleurs) ?
	lui 	$s0, 2
	beq 	$s0, $t7, first_workers_day
	j 	not_first_workers_day
first_workers_day :
		# si oui, on est désormais le 2nd jour des travailleurs
		lui 	$t7, 3
		j 	end

not_first_workers_day :
	# f = 4 (1er jour de l'industrie) ?
	lui 	$s0, 4
	beq	$s0, $t7, first_industry_day
	j 	not_first_industry_day
first_industry_day :
		# si oui, on est désormais le 2nd jour de l'industrie	
 		lui 	$t7, 5
		j 	end

not_first_industry_day :
	# f = 5 (2nd jour de l'industrie) ?
	lui 	$s0, 5
	beq 	$s0, $t7, second_industry_day
	j 	not_second_industry_day
second_industry_day :
		# si oui, on est désormais le 8 novembre
		lui 	$t7, 0
		lui 	$t3, 8
		lui 	$t6, 3
		j 	end

not_second_industry_day :
	# sinon on est désormais forcément le 1er du mois suivant
	lui 	$t7, 0
	lui 	$t3, 1
	lui 	$t6, 1
	addi 	$t4, $t4, 1	# on n'était pas en décembre
	j 	end

not_rest_day :		# le jour n'est pas férié
	# est-on le 7 novembre ?
	# j != 7 ?
	lui 	$s0, 7
	bne 	$t3, $s0, not_november_7th
	# m != 11 ?
	lui 	$s0, 11
	bne 	$t4, $s0, not_november_7th
	
	# si oui, on est maintenant le 1er jour de l'industrie
	# et on est notamment un jour férié
	lui	$t7, 4
	j 	now_rest_day

not_november_7th :	# on n'est pas le 7 novembre
	# j != 30 ?
	lui 	$s0, 30
	# si oui, on peut incrémenter le jour
	bne 	$s0, $t3, not_last_day

	# sinon, il faut déterminer si le lendemain est férié
	# m = 1 ?
	lui 	$s0, 1	
	beq 	$s0, $t4, january_30th
	j 	not_january_30th
january_30th :
		# si oui, on est maintenant le jour de Lénine
		lui	$t7, 1
		j 	now_rest_day	

not_january_30th :
	# m = 4 ?
	lui 	$s0, 4
	beq 	$s0, $t4, april_30th
	j 	not_april_30th
april_30th :
		# si oui, on est maintenant le 1er jour des travailleurs
		lui 	$t7, 2
		j 	now_rest_day

not_april_30th :
	# m = 2 ?
	lui	$s0, 2
	beq	$s0, $t4, february_30th
	j 	not_bissextile_february_30th
february_30th :
		# année bissextile ?
		# on calcule $t5 mod 4 en lui
		# enlevant 4 * $t5 / 4 (division eucluidienne)
		srl	$s0, $t5, 2
		sll 	$s0, $s0, 2
		beq 	$s0, $t5, bissextile_february_3Oth
		j 	not_bissextile_february_30th
bissextile_february_30th :
			# si oui, on est maintenant
			# le jour férié des années bissextiles
			lui 	$t7, 6
			j 	now_rest_day

not_bissextile_february_30th :
	# on est le 30 mais pas la veille d'un jour férié
	lui 	$t3, 1
	# m != 12 ?
	lui	$s0, 12
	bne 	$s0, $t4, not_last_day_of_year
	j 	last_day_of_year
not_last_day_of_year :
		# si oui, on incrémente seulement le mois
		addi	$t4, $t4, 1
		j	end
last_day_of_year :
	# sinon, on incrémente l'année et on remet le mois à 1
	addi 	$t5, $t5, 1
	lui	$t4, 1
	j 	end
	
	
not_last_second :	# s < 59
	addi	$t0, $t0, 1	# s ++
	j 	end
	
not_last_minute : 	# s = 59 & min < 59
	addi 	$t1, $t1, 1 	# min ++
	j	end
	
not_last_hour :		# min = 59 & h < 23
	addi 	$t2, $t2, 1 	# h ++
	j	end
	
not_last_day :		# h = 23 & j < 30
	addi 	$t3, $t3, 1	# j ++

	# t = 5 ?
	lui 	$s0, 5
	beq 	$s0, $t6, last_day_of_week
	j 	not_last_day_of_week
last_day_of_week :
		lui 	$t6, 1	# t = 1
		j 	end
not_last_day_of_week :
	addi	$t6, $t6, 1	# t ++
	j	end

now_rest_day :		# on est désormais un jour férié
	lui	$t3, 0		# j = 0
	lui 	$t6, 0		# t = 0

end :
