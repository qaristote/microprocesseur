open Ast
open Printer
open Unix

(* Wrong_type (equ_id, arg, type_obtained) *)
exception Wrong_type of ident * arg  * ty
(* Index_out_of_bounds equ_id *)
exception Out_of_bounds of ident
    
(* Variables set when the program is called through the command line *)
let print_only = ref false
let number_steps = ref (-1)
let memory_directory = ref "./"
let frequence = ref 0
let default = [|0; 0; 0; 1; 1; 0; 0; 0|]
let debug = ref false

(* Integer <-> boolean *)
let bool_of_bit = function
  | 0 -> false
  | 1 -> true
  | _ -> raise (Invalid_argument "bool_of_bit")

let bit_of_bool = function
  | false -> 0
  | true -> 1

let pow2 n = 1 lsl n

(* Prints a type fully. Used to print error messages. *)
let print_type ff t = match t with
  | TBit -> Format.fprintf ff "Bit"
  | TBitArray n -> Format.fprintf ff "BitArray %d" n

(* Outputs the value of an argument *)
let eval_arg env = function
  | Avar id -> Env.find id env
  | Aconst v -> v

(* Converts an address from BitArray to integer *)
let eval_addr env a = match eval_arg env a with
  | VBitArray t -> (* big-endian, but could be small-endian *)
      Array.fold_left (fun n b -> 2 * n + bit_of_bool b) 0 t
  | VBit b -> bit_of_bool b

(* Evaluates an equation *)              
let eval_expr env roms rams id = function
  | Earg a ->
      eval_arg env a
  | Ereg x -> (* Outputs the good value as scheduler.ml has 
                 _ = REG _ equations evaluated before everything else *)
      Env.find x env 
  | Enot a -> begin
      match eval_arg env a with
        | VBit b -> VBit (not b)
        | VBitArray t when Array.length t = 1 -> VBitArray [|not t.(0)|]
        | VBitArray t -> raise (Wrong_type (id, a, TBitArray (Array.length t)))
    end
  | Ebinop (op, a1, a2) -> begin
      match (eval_arg env a1, eval_arg env a2) with
        | (VBit b1, VBit b2) -> VBit (match op with
                                       | Or -> b1 || b2
                                       | And -> b1 && b2
                                       | Xor -> (b1 && not b2) || (not b1 && b2)
                                       | Nand -> not (b1 || b2))
        | (VBitArray t, _) -> raise (Wrong_type (id,
                                                 a1,
                                                 TBitArray (Array.length t)))
        | (_, VBitArray t) -> raise (Wrong_type (id,
                                                 a2,
                                                 TBitArray (Array.length t)))
    end
  | Emux (s, a1, a2) -> begin
      match eval_arg env s with
        | VBit true  | VBitArray [|true|]  -> eval_arg env a1
        | VBit false | VBitArray [|false|] -> eval_arg env a2
        | VBitArray t -> raise (Wrong_type (id, s, TBitArray (Array.length t)))
    end
  | Erom (_, word_size, read_addr) -> begin
      try
          VBitArray (Env.find id roms).(eval_addr env read_addr)
        with
          | Invalid_argument _ -> VBitArray (Array.make word_size false)
      end
  | Eram (_, _, read_addr, _, _, _) ->
      (* The writing part of this instruction is done after 
         all variables have seen their value computed. *)
      VBitArray (Env.find id rams).(eval_addr env read_addr)
  | Econcat (a1, a2) -> begin
      match eval_arg env a1, eval_arg env a2 with
        | VBitArray t1, VBitArray t2 -> VBitArray (Array.append t1 t2)
        | VBitArray t, VBit b -> VBitArray (Array.append t [|b|])
        | VBit b, VBitArray t -> VBitArray (Array.append [|b|] t)
        | VBit b1, VBit b2 -> VBitArray [|b1; b2|]
    end
  | Eslice (i1, i2, a) -> begin
      match eval_arg env a with
        | VBitArray t -> begin
            try
              VBitArray (Array.sub t i1 (i2 - i1 + 1))
            with
              | Invalid_argument _ -> raise (Out_of_bounds id)
          end
        | _ -> raise (Wrong_type (id, a, TBit))
    end
  | Eselect (i, a) -> begin
      match eval_arg env a with
        | VBitArray t -> begin
            try
              VBit t.(i)
            with
              | Invalid_argument _ -> raise (Out_of_bounds id)
          end
        | VBit b when i = 0 -> VBit b
        | _ -> raise (Out_of_bounds id)
    end

(* Asks the user to input variable <id> and outputs the value 
   that was read. Since this function is tail-recursive, there   
   shouldn't be any problem how much wrong inputs there are. *)
let rec read_value id t =
  let input_length = match t with
    | TBit | TBitArray 1 -> 1
    | TBitArray n -> n in
  Printf.printf "%s (%d bit%s) = "
    id
    input_length
    (if input_length <= 1 then "" else "s");
  let input = read_line () in
  try
    let v = Array.init
              (String.length input)
              (fun i -> match input.[i] with
                  | '0' -> false
                  | '1' -> true
                  | c -> raise (Invalid_argument ("character : "
                                                  ^ (String.make 1 c)))) in
    match input_length with
      | 1 when Array.length v = 1 -> VBit v.(0)
      | n when Array.length v = n -> VBitArray v
      | _ -> raise (Invalid_argument
                      (Format.sprintf "length : %d" (Array.length v)))
  with
    | Invalid_argument s -> Format.printf "Wrong input %s.@." s;
        read_value id t

(* Prints the variables to output in stdout. *)
let output env vars =
  List.iter
    (fun id -> try
       let value = (eval_addr env (Aconst (Env.find id env))) in
       if !debug then
         Format.printf "=> %s = %d = 0x%x@\n"
           id
           value
           value
       else
         Format.printf "%d " value
     with
       | Not_found -> Format.eprintf "Outbound output variable %s.@." id;
           exit 2)
    vars ;
  Format.printf "@."

let load_memory ?(ram_size = 0) in_channel word_size =
  let bits = ref [] in
  let size = ref 0 in
  begin try
    while true do
      let n = input_byte in_channel in
      for j = 7 downto 0 do
        bits := bool_of_bit ((n / (pow2 j)) mod 2) :: !bits ;
        size := !size + 1
      done ;
    done ;
  with
    | End_of_file -> () end ;
  close_in in_channel ;

  bits := List.rev !bits ;
  let amount_filled = !size / word_size in
  Array.append
    (Array.init
       amount_filled
       (fun i -> Array.init
                   word_size
                   (fun i ->
                      try
                        let result = List.hd !bits in
                        bits := List.tl !bits ;
                        result
                      with
                        | Failure _ -> false)))
    (Array.init
       (abs (ram_size - amount_filled))
       (fun i -> Array.make word_size false))

(* The main part of the program *)
let compile filename =

  (* We first check for any combinational cycle and order the netlist. *)
  let p = try
    Scheduler.schedule (Netlist.read_file filename)
  with 
    | Scheduler.Combinational_cycle ->
        Format.eprintf
          "The netlist has a combinatory cycle.@.";
        exit 2
    | Netlist.Parse_error s ->
        Format.eprintf
          "An error occurred during parsing : %s@."
          s;
        exit 2 in

  (* If the user asked to, we only write <filename>_sch.net.*)
  if !print_only then (
    let out =
      open_out ((Filename.chop_suffix filename ".net") ^ "_sch.net") in
    print_program out p;
    close_out out

    (* Otherwise we simulate the netlist. *)
  ) else ( 
    (* --- Initialization ---
       - env : stores variables 
       - roms : stores the ROM blocks
       - rams : stores the RAM blocks
       - i : counts the steps *)
    let env  = ref Env.empty in
    let roms = ref Env.empty in
    let rams = ref Env.empty in

    (* We go through the equations to initialize the memory. *)
    List.iter
      (fun (id, expr) -> match expr with
          | Ereg x ->
              (* Give a default value to each variable
                 that is the argument of a register. *)
              env := Env.add
                       x
                       (match Env.find id p.p_vars with
                         | TBit -> VBit false
                         | TBitArray n -> VBitArray (Array.make n false))
                       !env

          | Erom (_, word_size, _) ->
              (* Loads the ROM block associated 
                 to each variable whose value is
                 read through a ROM statement. *)
              let file = (* Tries to open <id>.rom *)
                try
                  open_in_bin (!memory_directory ^ id ^ ".rom")
                with
                  | Sys_error s when (s = !memory_directory ^
                                          id ^
                                          ".rom: No such file or directory"
                                      || s = "Is a directory") ->
                      Format.eprintf
                        "%s.rom either does not exist or is a directory."
                        id;
                      exit 2 in
              let r = load_memory file word_size in
              roms := Env.add id r !roms

          | Eram (addr_size, word_size, _, _, _, _) ->
              (* Loads the RAM block associated 
                 to each variable whose value is
                 read through a RAM statement. *)
              let ram_size = pow2 addr_size in
              let r = try
                let file = (* Tries to open <id>.ram *)
                  open_in_bin (!memory_directory ^ id ^ ".ram") in
                load_memory file word_size ~ram_size: ram_size
              with
                | Sys_error s when (s = !memory_directory ^
                                        id ^
                                        ".ram: No such file or directory"
                                    || s = "Is a directory") ->
                    Array.init
                      ram_size
                      (fun i -> Array.make word_size false) in
              
              (* only for the clock, useless elsewhere ;
                 needs to be changed everytime microprocesseur.mj is changed *)
              if List.mem id ["t0" ; "t1" ; "t2" ; "t3" ;
                              "t4" ; "t5" ; "t6" ; "t7" ;
                              "_l_317_1614" ; "_l_321_1611"] then (
                for i = 8 to 15 do
                  for j = 0 to word_size - 1 do
                    r.(i).(j) <- bool_of_bit ((default.(i - 8) /
                                               (pow2 (word_size - j - 1)))
                                              mod 2)
                  done ;
                done) ;
              
              rams := Env.add id r !rams
          | _ -> ())
      p.p_eqs;

    let i = ref 0 in
    let start_time = ref (Unix.time ()) in
    while !i <> !number_steps do
      (* --- Input ---
           For each variable in p.p_inputs, we 
           ask for its value and add it to env *)

      env := (List.fold_left
                (fun tmp id -> Env.add
                                 id
                                 (read_value id (Env.find id p.p_vars))
                                 tmp)
                !env
                p.p_inputs);

      (* --- Execution ---
         For each equation in p.p_eqs, we evaluate it 
         and update the associated variable in env.
         For _ = RAM _ equations, we first compute 
         the values of all the variables for the
         current cycle and only then write into the RAM. *)

      begin try
        (* We update the value of all the variables. *)
        env := List.fold_left
                 (fun tmp (id, expr) -> Env.add
                                          id
                                          (eval_expr tmp !roms !rams id expr)
                                          tmp)
                 !env
                 p.p_eqs;
        (* Only now that we know every variable 
           can we write in the RAM blocks. *)
        List.iter
          (fun (id, expr) ->
             match expr with
               | Eram (_, word_size, _, write_enable, write_addr, data) ->
                   if !debug then (
                     Format.printf "%s@." id ;
                     Array.iter
                       (fun t -> Format.printf "%a@ " print_value (VBitArray t))
                       (Env.find id !rams)) ;
                   begin
                   match eval_arg !env write_enable with
                     | VBit true -> begin
                         match eval_arg !env data with
                           | VBit b ->
                               if word_size = 1 then
                                 (Env.find id !rams).(eval_addr !env write_addr)
                                 <- [|b|]
                               else
                                 raise (Wrong_type (id,
                                                    data,
                                                    TBit))
                           | VBitArray t ->
                               if word_size = Array.length t then
                                 (Env.find id !rams).(eval_addr !env write_addr)
                                 <- t
                               else
                                 raise (Wrong_type (id,
                                                    data,
                                                    TBitArray (Array.length t)))
                       end
                     | VBit false -> ()
                     | VBitArray t ->
                         raise (Wrong_type (id,
                                            write_enable,
                                            TBitArray (Array.length t)))
                 end
               | _ -> ())
          p.p_eqs;
      with
        | Wrong_type (id, a, t) ->
            (* This exception should never be caught unless the user wrote 
               or modified the netlist manually, without MiniJazz. *)
            Format.eprintf
              "While evaluating %s, %a was expected to be of type %a but was of type %a.@."
              id
              print_arg a
              (fun ff arg -> match arg with
                  | Aconst (VBit _) ->
                      print_type ff TBit
                  | Aconst (VBitArray t) ->
                      print_type ff (TBitArray (Array.length t))
                  | Avar x ->
                      print_type ff (Env.find x p.p_vars)) a
              print_type t;
            exit 2
      end;

      (* --- Update --- *)
      i := 1 + !i;
      if !frequence > 0 then (
        if (!i - 1) mod !frequence = 0 then (
          let end_time = Unix.time () in
          Unix.sleepf (1. -. end_time +. !start_time);
          start_time := end_time ;

      (* --- Output ---
         For each variable in p.p_outputs, we print it in stdout. 
         We do so iff !frequence cycles have been done since the last output. *)
          output !env ((if !debug then
                         ["PC" ; "command" ; "ops" ]
                       else
                         [])@p.p_outputs)
      ) else
          output !env p.p_outputs)
    done)

let main () =
  Arg.parse
    ["-p", Arg.Set print_only, " " ;
     "--print", Arg.Set print_only, ": prints the result of scheduling in a file and stops." ;
     "-n", Arg.Set_int number_steps, "<number of steps> : specifies the number of cycles to simulate.\n     If not specified, the netlist is simulated indefinitely." ;
     "-m", Arg.Set_string memory_directory, " " ;
     "--memory", Arg.Set_string memory_directory, "/path/to/directory : location of the memory files. By default, set to './'.\n           The path of the directory must contain '/' at its end. \n           There must be one <var>.rom file for each variable <var> that accesses the ROM.\n           <var>.ram files are optional.\n           Each file should be a binary file, that is a text file where character n is the ASCII character encoded by the byte ROM[8*n]...ROM[8*n+7]." ;
     "-f", Arg.Set_int frequence, " " ;
     "--freq", Arg.Set_int frequence, "<frequence> : number of cycles per second." ;
     "-t", Arg.Tuple (List.init
                        8
                        (fun i ->
                           Arg.Int (fun n ->
                                     default.(i) <- n))), " " ;
     "--time", Arg.Tuple (List.init
                        8
                        (fun i ->
                           Arg.Int (fun n ->
                                     default.(i) <- n))),
     "<t0> <t1> <t2> <t3> <t4> <t5> <t6> <t7> : sets the time at the beginning of the simulation." ;
     "-d", Arg.Set debug, " " ;
     "--debug", Arg.Set debug, ": prints a handful of useful information."]
    compile
    "" ;;

main ()
