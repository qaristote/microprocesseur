open Ast
open Graph

exception Combinational_cycle

(* We create the graph of the dependencies 
   of the equations and order it so that we know 
   in which order we must execute our equations. *)
let schedule p =
  let vars = mk_graph () in
  (* We first add a node for each variable in the netlist. *)
  Env.iter (fun v _ -> add_node vars v) p.p_vars ;
  (* Then we add the appropriate edges for each equation. *)
  List.iter
    (fun (id, expr) ->
       let add_var = function
         | Avar x -> add_edge vars x id
         | _ -> () in
       match expr with
         | Earg a ->
             add_var a
         | Ereg x -> 
             (* This leads to a combinational cycle when
                there isn't one if the netlist contains 
                two equations similar to the following :
                > a = REG b
                > b = REG a   
                It's not that much of a problem as those
                netlists are pretty useless. *)
             add_edge vars id x
         | Enot a ->
             add_var a
         | Ebinop (_, a, b) ->
             add_var a ;
             add_var b
         | Emux (s, a, b) -> add_var s;
             add_var a;
             add_var b
         | Erom (addr_size, word_size, read_addr) ->
             add_var read_addr
         | Eram (addr_size, word_size, read_addr,
                 write_enable, write_addr, data) ->
             (* We do not need to know write_enable, 
                write_addr and data to compute the output. *)
             add_var read_addr
         | Econcat (a, b) ->
             add_var a;
             add_var b
         | Eslice (_, _, a) ->
             add_var a
         | Eselect (_, a) ->
             add_var a)
    p.p_eqs ;
    let rec gen_eqs = function
      | [] -> []
      | t::q -> try
        (t, List.assoc t p.p_eqs)::(gen_eqs q)
      with
        | Not_found -> gen_eqs q in
    try
      {p_eqs = gen_eqs (topological vars); 
       p_inputs = p.p_inputs;
       p_outputs = p.p_outputs;
       p_vars = p.p_vars}
    with Has_cycle -> raise Combinational_cycle
