# Netlist simulator

This program takes a netlist (.net) file and simulates its execution.

# Requirements

Version 4.03.0 of OCaml is required for the program to compile, as `Unix.sleepf`
had not been introduced before.
To update your version of OCaml, use
```
opam switch 4.03.0
```
(replace 4.03.0 by the version you want, you can see available versions with 
`opam switch`).
Then, as suggested, use
```
eval `opam config env`
```
Everything should work from there.

# Building

Use `make`.

# Running

Use 
```
./netlistsim <file name>.net
```
Options are:
```
-p
--print : prints the result of scheduling in a file and stops.
-n <number of steps> : specifies the number of cycles to simulate.
   If not specified, the netlist is simulated indefinitely.
-m
--memory /path/to/directory : location of the memory files. By default, set to './'.
         The path of the directory must contain '/' at its end. 
         There must be one <var>.rom file for each variable <var> that accesses the ROM.
         <var>.ram files are optional.
         Each file should be a binary file, that is a text file where character n is the ASCII character encoded by the byte ROM[8*n]...ROM[8*n+7].
-f
--freq <frequence> : number of cycles per second.
-t
--time <t0> <t1> <t2> <t3> <t4> <t5> <t6> <t7> : sets the time at the beginning of the simulation.
-d
--debug : prints a handful of useful information.
-help
--help : display this list of options
```

Some netlist examples can be found in the ```test``` directory.

# Contributing

The `.ocp-indent` file is provided to set the default indentations rules. It is
recommended to use it. To do so, you only need to have ocp-indent installed :
```
opam install ocp-indent
opam user-setup install
```
should do the work and configure your editor provided you are not using anything
far-fetched. If this does not work, you can refer to [ocp-indent's own documentation](https://www.typerex.org/ocp-indent.html).

# Acknowledgment

This program was built upon [Timothy Bourke](https://github.com/tbrk)'s [MiniJazz exercices](https://github.com/inria-parkas/minijazz/tree/master/netlist).
