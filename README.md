# Microprocessor

This project is part of the *Système Digital* course at the *École Normale Supérieure de Paris*. Its aim is to simulate a microprocessor running a clock in the revolutionnary soviet calendar.

It contains a graphical interface programmed in Python, a netlist simulator programmed in OCaml, the description of the microprocessor in MiniJazz as well as a MIPS assembly program computing the new time after adding one second.
