\documentclass{article}
\usepackage{fontspec}
\usepackage{graphicx}
\usepackage{hyperref}

\renewcommand{\arraystretch}{1.3}

\title{De la création d'un microprocesseur}
\author{Quentin Aristote, Félix Breton, Lucie Galland, Léa Naccache}
\date{31 janvier 2019}

\begin{document}

\maketitle

\section*{Introduction}

Le programme fourni simule l'éxecution d'une horloge dans le calendrier révolutionnaire soviétique par un microprocesseur dont l'architecture est dérivée de l'architecture MIPS.

\section*{Le calendrier révolutionnaire soviétique}

Le calendrier révolutionnaire soviétique est constitué de 12 mois, eux-mêmes constitués de 6 semaines de 5 jours. Chaque jour de la semaine correspond à un groupe de travailleur dont c'est le jour de repos. On compte de plus 5 jours fériés (6 les années bissextiles) :
\begin{itemize}
\item le jour de Lénine après le 30 janvier
\item deux jours du travail aptrès le 30 avril
\item deux jours de l'industrie après le 7 novembre
\item les années bissextiles, un jour férié supplémentaire après le 30 février
\end{itemize}

\section*{Le microprocesseur}

\subsection*{Architecture}

Le microprocesseur contient 16 registres de 16 bits, en plus de deux registres qui contiennent respectivement l'adresse de lecture dans le programme assembleur executé (32 bits) et un compteur du nombre d'opérations effectuées (7 bits).
\\ Les registres sont stockés dans le RAM. Il y a donc en pratique 10 blocs de RAM contenant les mêmes données afin de pouvoir accéder à 10 registres à chaque cycle : 8 registres contenant la date et l'heure actuelle et 2 registres permettant d'effectuer les calculs.

\bigskip \noindent Par défaut, le microprocesseur est initialisé pour effectuer 50 cycles par secondes.

\begin{figure}
  \includegraphics[width=1.\linewidth]{circuit.pdf}
  \caption{Schéma de l'architecture du microprocesseur}
\end{figure}

\noindent À chaque cycle d'horloge,
\begin{itemize}
\item on lit dans la ROM la prochaine instruction à éxecuter ;
\item on lit les valeurs des registres nécessaires ;
\item on calcule en parallèle les résultats de chaque instruction possible et on sélectionne le bon grâce à un jeu de multiplexeurs ;
\item on met à jour les registres ainsi que l'adresse de lecture dans la ROM.
\end{itemize}


\subsection*{Jeu d'instruction}

On implémente un sous-ensemble des instructions MIPS, parfois traitées de manière légèrement différentes. Elles se divisent en trois catégories. Tous les entiers sont représentés par des entiers signés.
\\ Hormis les branchements (ça ne devrait pas être le cas selon la documentation de l'assembleur MIPS mais en pratique on observe que ce n'est pas le cas pour les branchements), toutes les instructions sont précédées de
\begin{center}
  \texttt{PC <- PC + 4}
\end{center}
\subsubsection*{Instructions de type R}

Les instructions de type R sont encodées ainsi :
\begin{center}
  \begin{tabular}{| c | c | c | c | c | c |}
    \hline
    opcode & rs & rt & rd & shamt & funct \\
    \hline \hline
    6 bits & 5 bits & 5 bits & 5 bits & 5 bits & 6 bits \\
    \hline
  \end{tabular}
\end{center}


\bigskip \noindent Les instructions sont les suivantes :
\begin{center}
  \begin{tabular}{| c | c | l | l |}
    \hline
    opcode & funct & Syntaxe & Effet \\
    \hline \hline
    \texttt{000000} & \texttt{100000} & \texttt{add rd, rs, rt} & \texttt{rd <- rs + rt} \\
    \hline
    \texttt{000000} & \texttt{000000} & \texttt{sll rd, rt, shamt} & \texttt{rd <- rs << shamt} \\
    \hline
    \texttt{000000} & \texttt{000010} & \texttt{srl rd, rt, shamt} & \texttt{rd <- rs << shamt} \\
    \hline
  \end{tabular}
\end{center}

\subsubsection*{Instructions de type I}

Les instructions de type R sont encodées ainsi :
\begin{center}
  \begin{tabular}{| c | c | c | c |}
    \hline
    opcode & rs & rt & rd \\
    \hline \hline
    6 bits & 5 bits & 5 bits & 16 bits \\
    \hline
  \end{tabular}
\end{center}


\bigskip \noindent Les instructions sont les suivantes :
\begin{center}
  \begin{tabular}{| c | l | l |}
    \hline
    opcode & Syntaxe & Effet \\
    \hline \hline
    \texttt{001000} & \texttt{add rs, rt, imm} & \texttt{rt <- rs + imm} \\
    \hline 
    \texttt{000001} & \texttt{bltz rs, imm} & si \texttt{rs} $< 0$, \texttt{PC <- PC + imm} \\
    \hline
    \texttt{000100} & \texttt{beq rs, rt, imm} & si \texttt{rs} $=$ \texttt{rt}, \texttt{PC <- PC + imm} \\
    \hline
    \texttt{000101} & \texttt{bne rs, rt, imm} & si \texttt{rs} $\neq$ \texttt{rt}, \texttt{PC <- PC + imm} \\
    \hline
    \texttt{001111} & \texttt{lui rt, imm} & \texttt{rt <- imm} \\
    \hline
  \end{tabular}
\end{center}

\subsubsection*{Instructions de type J}

Les instructions de type R sont encodées ainsi :
\begin{center}
  \begin{tabular}{| c | c |}
    \hline
    opcode & pseudo-address\\
    \hline \hline
    6 bits & 26 bits \\
    \hline
  \end{tabular}
\end{center}


\bigskip \noindent On utilise une seule instruction de ce type :
\begin{center}
  \begin{tabular}{| c | l | l |}
    \hline
    opcode & Syntaxe & Effet \\
    \hline \hline
    \texttt{000010} & \texttt{j label} & \texttt{PC <- PC[0..3].pseudo-address.OO} \\
    \hline
  \end{tabular}
\end{center}

\bigskip \noindent En pratique on soustrait toujours l'adresse \texttt{0x00400024} à \texttt{PC} après un saut car le compilateur utilisé considère que l'éxecution du programme commence à cette adresse alors qu'en réalité elle se fait à l'adresse \texttt{0x00000000}.

\section*{L'horloge}

L'horloge est un programme assembleur utilisant les seules instructions ci-dessus calculant les modifications de la date et de l'heure à l'ajout d'une seconde. En plus de \texttt{\$s0} pour les calculs intermédiaires, on utilise 8 registres pour stocker ces informations :
\begin{itemize}
\item \texttt{\$t0} ($0$ à $59$) contient les secondes
\item \texttt{\$t1} ($0$ à $59$) contient les minutes
\item \texttt{\$t2} ($0$ à $23$) contient les heures
\item \texttt{\$t3} ($0$ à $30$) contient le jour ; la valeur $0$ est utilisée lorsque le jour est férié
\item \texttt{\$t4} ($1$ à $12$) contient le mois
\item \texttt{\$t5} ($0$ à $2^{15}-1$) contient l'année
\item \texttt{\$t6} ($0$ à $5$) contient le jour de la semaine ; la valeur $0$ est utilisée lorsque le jour est férié
\item \texttt{\$t7} ($0$ à $6$) contient le jour férié ; la valeur $0$ est utilisée lorsque le jour n'est pas férié
\end{itemize}

\noindent À chaque cycle d'horloge on incrémente les secondes et subséquemment les minutes, les heures, ...
\\ Le calcul des années bissextiles (\textit{id est} un calcul modulo $4$) se fait à l'aide d'un \textit{logical shift left} suivi d'un \textit{logical shift right}, à chaque fois sur 2 bits.

\bigskip \noindent On transforme le programme assembleur en fichier binaire à l'aide du logiciel \href{http://spimsimulator.sourceforge.net/#copyright}{\texttt{spim}}.

\section*{Le simulateur de netlist}

Le simulateur de netlist proposé ne supporte pas les netlists du type
\begin{center}
  \texttt{a = REG b}
  \\ \texttt{b = REG a}
\end{center}
afin d'optimiser la simulation de la netlist. Ce n'est pas une grande perte car ce genre de netlist n'implémente rien de très interessant.
\\ \noindent Pour chaque déclaration du type
\begin{center}
  \texttt{a = RAM ...}
\end{center}
on crée un bloc de RAM distinct. De même, pour chaque déclaration du type
\begin{center}
  \texttt{a = ROM ...}
\end{center}
on crée un bloc de ROM distinct. Si le contenu d'un bloc de ROM est toujours lu dans un fichier \texttt{<id>.rom} (qui se trouve lui-même dans le dossier fourni par le paramètre \texttt{--memory}), l'équivalent pour les RAM est simplement optionnel : si le fichier \texttt{<id>.ram} n'est pas fourni, on initialise la RAM à $0$.

\bigskip \noindent C'est le simulateur de netlist qui s'assure que l'horloge est synchronisée avec le temps réel : via le paramètre \texttt{--freq}, on lui indique le nombre de cycles qui doivent êtres simulés chaque seconde. En pratique, on observe que si cette méthode est très peu précise à la seconde près, elle semble être suffisemment précise à la minute près (le décalage reste borné, de l'ordre de la seconde). Si cette option n'est pas spécifié, l'horloge s'éxecute à sa vitesse maximale.

\bigskip \noindent On peut finalement se limiter au tri topologique de la netlist à l'aide de \texttt{--print}, spécifier le nombre de cycles à simuler via le paramètre \texttt{-n} (s'il n'est pas spécifié on en simule une infinité), définir le temps au début de la simulation grâce à \texttt{--time} et montrer les détails de la simulation à l'aide de \texttt{--debug}. Sans cette option, le simulateur écrit à chaque seconde dans \texttt{stdout} les valeurs des registres \texttt{\$t0} à \texttt{\$t7} dans cet ordre et séparés par un espace.

\begin{figure}
  \centering
\begin{verbatim}
-p
--print : prints the result of scheduling in a file and stops.
-n <number of steps> : specifies the number of cycles to simulate.
   If not specified, the netlist is simulated indefinitely.
-m
--memory /path/to/directory : location of the memory files. 
         By default, set to './'.
         The path of the directory must contain '/' at its end. 
         There must be one <var>.rom file for each variable <var> 
         that accesses the ROM.
         <var>.ram files are optional.
         Each file should be a binary file, that is a text file 
         where character n is the ASCII character encoded by the 
         byte ROM[8*n]...ROM[8*n+7].
-f
--freq <frequence> : number of cycles per second.
-t
--time <t0> <t1> <t2> <t3> <t4> <t5> <t6> <t7> : sets the time at 
       the beginning of the simulation.
-d
--debug : prints a handful of useful information.
-help
--help : display this list of options
\end{verbatim}
  \caption{Message d'aide affiché via \texttt{--help}}
\end{figure}

\bigskip \noindent Pour simuler une horloge commençant au 27 janvier 2019 à 23:55:36, on utilise donc depuis le dossier principal 

\begin{center}
  \begin{tabular}{c l}
    \texttt{netlist-simulator/netlistsim} & \texttt{--memory clock/} \\
                                          & \texttt{--freq 50} \\
                                          & \texttt{--time 36 55 23 27 1 2019 2 0} \\
                                          & \texttt{microprocessor/microprocesseur.mj}
  \end{tabular}
\end{center}

\section*{Interface}

\end{document}