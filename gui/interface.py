from tkinter import *
import time
import os

'''
----------      For your information     ------------

t0 = 0  # seconde (s) (0 -> 59)
t1 = 1  # minute (min) (0 -> 59)
t2 = 2  # heure (h) (0 -> 23)
t3 = 3  # jour (j) (1 -> 30)
t4 = 4  # mois (m) (1 -> 12)
t5 = 2000  # année (a) (0 -> 2^31-1)
t6 = 0  # groupe de travailleurs (t) (0 -> 5)
t7 = 5  # jour férié (f) (0 -> 6)

----------------------------------------------------
'''


def Mois_chiffre_to_str(t):
    if t == 1:
        mois = "Janvier"
    elif t == 2:
        mois = "Fevrier"
    elif t == 3:
        mois = "Mars"
    elif t == 4:
        mois = "Avril"
    elif t == 5:
        mois = "Mai"
    elif t == 6:
        mois = "Juin"
    elif t == 7:
        mois = "Juillet"
    elif t == 8:
        mois = "Aout"
    elif t == 9:
        mois = "Septembre"
    elif t == 10:
        mois = "Octobre"
    elif t == 11:
        mois = "Novembre"
    elif t == 12:
        mois = "Decembre"
    else:
        mois = -1
    return mois


def Mois_stg_to_chifre(t):
    if t == 'Jan':
        mois = 1
    elif t == "Fevrier":
        mois = 2
    elif t == "Mars":
        mois = 3
    elif t == "Avril":
        mois = 4
    elif t == "Mai":
        mois = 5
    elif t == "Juin":
        mois = 6
    elif t == "Juillet":
        mois = 7
    elif t == "Aout":
        mois = 8
    elif t == "Septembre":
        mois = 9
    elif t == "Octobre":
        mois = 10
    elif t == "Novembre":
        mois = 11
    elif t == "Decembre":
        mois = 12
    else:
        mois = -1
    return mois


def Couleur(t):
    if t == 0:
        couleur = "white"
    elif t == 1:
        couleur = "brown"
    elif t == 2:
        couleur = "blue"

    elif t == 3:
        couleur = "yellow"

    elif t == 4:
        couleur = "red"
    else:
        couleur = "green"

    return couleur


def Jour_ferié_to_str(t):
    if t == 1:
        jour_f = "Jour de Lenine"
    elif t == 2:
        jour_f = "Premiere journée du travail"
    elif t == 3:
        jour_f = "Deuxième journée du travail"
    elif t == 4:
        jour_f = "Première journée de l'industrie est Deuxième journée de l'industrie"
    else:
        jour_f = "Le 31 février"
    return jour_f


def Jour_ferié_to_nb(t):
    if t == "Jour de Lenine":
        jour_f = 1
    elif t == "Premiere journée du travail":
        jour_f = 2
    elif t == "Deuxième journee du travail":
        jour_f = 3
    elif t == "Premiere journee de l'industrie est Deuxieme journee de l'industrie":
        jour_f = 4
    elif t == "Le 31 fevrier":
        jour_f = 5
    return jour_f


def interface(t0, t1, t2, t3, t4, t5, t6, t7):
    if t7 == 0:
        mois = Mois_chiffre_to_str(t4)
        couleur = Couleur(t6)
        date = "Le {} {} {}".format(t3, mois, t5)

    else:
        couleur = Couleur(t6)
        jour_f = Jour_ferié_to_str(t7)
        date = "{} {}".format(jour_f, t5)

    heure = "{}h {}min {}sec".format(t2, t1, t0)
    texte = (couleur, date, heure)

    return texte


def Recupere_mot(string):
    i = 0
    tmp = i
    mot = []
    while i <= len(string):
        if string[i - 1] == " ":
            mot.append(string[tmp:i - 1])
            tmp = i
        i += 1
    mot.append(string[tmp:-1])
    return mot


def Recupere_heure(string):
    i = 0
    tmp = i
    heure = []
    while i <= len(string):
        if string[i - 1] == ":":
            heure.append(string[tmp:i - 1])
            tmp = i
        i += 1
    heure.append(string[tmp:])
    return heure


if __name__ == '__main__':

    date_chiffre = False

    if date_chiffre:
        # Si on rentre les valeurs au format '03.25.60' , les valeurs sont rentrées par l'utilisateur
        heure = input("Entrer une heure au format '03.25.60': ")
        date = input("Entrer une date 'JourMoisAnnée': ")
        t0 = int(heure[4:6])
        t1 = int(heure[2:4])
        t2 = int(heure[0:2])

        t3 = int(date[0:2])
        t4 = int(date[2:4])
        t5 = int(date[4:8])

        t6 = int(input("Entrer de gr de travailleur [0:5]:  "))
        t7 = 0  # jour férié (f) (0 -> 6)

        mois = Mois_chiffre_to_str(t4)
        print(mois)

    if not date_chiffre:
        # Si on rentre les valeurs au format 'Mar 22 jan 2019 14:09:19 CET '

        test = os.popen("date").read()
        mot = Recupere_mot(test)
        print(mot)

        heure = Recupere_heure(mot[3])
        print(heure)
        print(mot[3])
        t0 = heure[2]
        t1 = heure[1]
        t2 = heure[0]

        t3 = mot[2]
        t4 = mot[1]
        t5 = mot[5]

        t6 = int(input("Entrer de gr de travailleur [0:5]:  "))
        t7 = 0  # jour férié (f) (0 -> 6)

        mois = str(t4)
        print(mois)

    couleur = Couleur(t6)

    fenetre = Tk()
    fenetre.title('HORLOGE')
    date = LabelFrame(fenetre, text="Le {} {} {}".format(t3, mois, t5), padx=40, pady=40, bg=couleur)
    heure = Label(date, text="{}h {}min {}sec".format(t2, t1, t0), bg=couleur)
    date.pack(fill="both", expand="yes")
    heure.pack(fill="both", expand="yes")

    while True:
        # Recupérer les valeurs t0,t1...
        t6 = (t6 + 1) % 6
        test = os.popen("date").read()
        mot = Recupere_mot(test)
        t4 = Mois_stg_to_chifre(mois)
        couleur, Date, Heure = interface(t0, t1, t2, t3, t4, t5, t6, t7)

        date.configure(text=Date, bg=couleur)
        heure.configure(text=Heure, bg=couleur)
        fenetre.update_idletasks()
        fenetre.update()
        time.sleep(1)
    fenetre.mainloop()

    # interface(t0, t1, t2, t3, t4, t5, t6, t7)
    fenetre.destroy()
    exit()
